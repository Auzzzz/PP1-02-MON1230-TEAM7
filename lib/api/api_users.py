
from flask import Flask, Blueprint, request, jsonify, render_template, g, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os, requests, json, pytz
from datetime import datetime
from lib.models.user_model import UserModel as U
from lib.site.user_functions import UsersFunc as UF
from lib.models.data_model import DataModel as D
from lib.o import db, ma, jwt
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity,
    get_jwt
)

api_users = Blueprint("api_users", __name__, url_prefix='/api/v1')

# Temp Endpoint
@api_users.route("/")
def dbcreation():
    """Gets all people registereda
    :return: Returns all people registered
   
    """
    db.create_all()
    return "Hi"

@api_users.route("/a")
def a_users():
    
    return U.test()


@api_users.route("/input",  methods=['POST'])
@jwt_required()
def api_input():
    print(get_jwt())
    tz_AU = pytz.timezone('australia/melbourne') 
    uuid = get_jwt_identity()
    request_data = request.get_json()
    utc_time = datetime.utcnow()
    time = pytz.utc.localize(utc_time, is_dst=None).astimezone(tz_AU)
    entry_count = 0

    for each in request_data:
        print(request_data[each])
        print(each)
        print(entry_count)
        new_entry = D(
            uuid = 0,
            device_uuid = uuid,
            date_time = utc_time,
            sensor_name = each,
            value = request_data[each]
        )

        try:
                new_entry.save_to_db()
                entry_count =+ 1
            
        except Exception as e:
            msg = "An error has occured"
            emsg = e
            return jsonify(msg = msg, emsg = emsg)


    return "Hi"




