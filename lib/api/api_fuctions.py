from lib.models.user_tokens_revoked import UserRevokedTokensModel as UR

def check_token(jti):
    return UR.check_token(jti)