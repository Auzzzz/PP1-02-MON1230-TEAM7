from lib.o import db, ma
from flask import jsonify

class User_TokensModel(db.Model):
    __tablename__ = "user_tokens"
    uuid = db.Column(db.VARCHAR(50), primary_key = True)
    owner_uuid = db.Column(db.VARCHAR(50))
    token = db.Column(db.String(287))
    device_site = (db.Column(db.Integer))
    location = db.Column(db.VARCHAR(50))
    time_limit = db.Column(db.Integer)
    status = db.Column(db.Integer)
    sensor_names = db.Column(db.VARCHAR(500))

    def __init__(self, uuid, owner_uuid, token, location, device_site, time_limit, status):
        self.uuid = uuid
        self.owner_uuid = owner_uuid
        self.token = token
        self.location = location
        self.device_site = device_site
        self.time_limit = time_limit
        self.status = status

    def show_all():

        return User_TokensModel.query.all()
        
    def find_by_uuid(uuid):

        return User_TokensModel.query.filter_by(uuid=uuid).first()

    def check_uuid(uuid):
        if User_TokensModel.query.filter_by(uuid=uuid).first:
            return False

        return True
    
    def check_owner(owner_uuid):

        return User_TokensModel.query.filter_by(owner_uuid=owner_uuid).all()

    def find_site(device_site):

        return User_TokensModel.query.filter_by(device_site=device_site).all()

    def update(uuid, value):
        device = User_TokensModel.query.filter_by(uuid=uuid).first()
        device.status = value
        db.session.commit()
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()
