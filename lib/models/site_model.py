from lib.o import db, ma
from flask import jsonify

class Site_Model(db.Model):
    __tablename__ = "user_sites"
    uuid = db.Column(db.Integer, primary_key = True, autoincrement=True)
    name = db.Column(db.VARCHAR(100))
    desc = db.Column(db.String(287))

    def __init__(self, uuid, name, desc):
        self.uuid = uuid
        self.name = name
        self.desc = desc

    def show_all():

        return Site_Model.query.all()
        
    def find_by_uuid(uuid):

        return Site_Model.query.filter_by(uuid=uuid).first()


    def update(uuid, name, desc):
        site = Site_Model.query.filter_by(uuid=uuid).first()
        site.name = name
        site.desc = desc
        db.session.commit()
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()
