from lib.o import db, ma
from sqlalchemy import desc

class DataModel(db.Model):
    __tablename__ = "sensor_data"
    uuid = db.Column(db.Integer, primary_key = True, autoincrement=True)
    device_uuid = db.Column(db.VARCHAR(50))
    date_time = db.Column(db.DateTime)
    sensor_name = db.Column(db.Text)
    value = db.Column(db.VARCHAR(4))

    def __init__(self, uuid, device_uuid, date_time, sensor_name, value):
        self.uuid = uuid
        self.device_uuid = device_uuid
        self.date_time = date_time
        self.sensor_name = sensor_name
        self.value = value
   

    def find_everyone():

        return DataModel.query.all()

    def find_by_uuid(username):

        return DataModel.query.filter_by(uuid=uuid).first()
    
    
    def find_by_date_time(date_time):

        return DataModel.query.filter_by(date_time=date_time).first()

    def find_by_uuid(uuid):

        return DataModel.query.filter_by(uuid=uuid).first()

    def last_upload(device_uuid):

        return DataModel.query.filter_by(device_uuid=device_uuid).order_by(DataModel.date_time.desc()).first()

    def graph_data(device_uuid):

        return DataModel.query.filter_by(device_uuid=device_uuid).order_by(DataModel.date_time.desc()).all()

    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()