from lib.o import db, ma

class TicketModel(db.Model):
    __tablename__ = "tickets"
    uuid = db.Column(db.Integer, primary_key = True, autoincrement=True)
    user_uuid = db.Column(db.VARCHAR(36))
    date_time = db.Column(db.DateTime)
    ticket_header = db.Column(db.Text)
    ticket_body = db.Column(db.Text)
    ticket_status = db.Column(db.Integer)  

    def __init__(self, uuid, user_uuid, date_time, ticket_header, ticket_body, ticket_status):
        self.uuid = uuid
        self.user_uuid = user_uuid
        self.date_time = date_time
        self.ticket_header = ticket_header
        self.ticket_body = ticket_body
        self.ticket_status = ticket_status
   

    def find_all():

        return TicketModel.query.all()

    def find_by_uuid(uuid):

        return TicketModel.query.filter_by(uuid=uuid).first()
    
    
    def find_by_date_time(date_time):

        return TicketModel.query.filter_by(date_time=date_time).first()

    def find_by_user_uuid(uuid):

        return TicketModel.query.filter_by(user_uuid=uuid).all()
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()