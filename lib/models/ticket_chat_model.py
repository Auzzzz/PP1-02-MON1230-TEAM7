from lib.o import db, ma

class TicketChatModel(db.Model):
    __tablename__ = "tickets_replys"
    uuid = db.Column(db.Integer, primary_key = True)
    ticket_uuid = db.Column(db.Integer)
    user_uuid = db.Column(db.VARCHAR(36))
    date_time = db.Column(db.DateTime)
    ticket_reply = db.Column(db.Text)

    def __init__(self, uuid, ticket_uuid, user_uuid, date_time, ticket_reply):
        self.uuid = uuid
        self.ticket_uuid = ticket_uuid
        self.user_uuid = user_uuid
        self.date_time = date_time
        self.ticket_reply = ticket_reply

    def find_all():

        return TicketChatModel.query.all()

    def find_by_uuid(uuid):

        return TicketChatModel.query.filter_by(uuid=uuid).first()
    
    
    def find_by_date_time(date_time):

        return TicketChatModel.query.filter_by(date_time=date_time).first()

    def find_by_user_uuid(uuid):

        return TicketChatModel.query.filter_by(user_uuid=uuid).all()

    def find_by_ticket_uuid(ticket_uuid):

        return TicketChatModel.query.filter_by(ticket_uuid=ticket_uuid).all()
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()