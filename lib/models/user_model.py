from lib.o import db, ma
from flask import jsonify

class UserModel(db.Model):
    __tablename__ = "user"
    uuid = db.Column(db.VARCHAR(36), primary_key = True)
    first_name = db.Column(db.VARCHAR(50))
    last_name = db.Column(db.VARCHAR(50))
    email_address = db.Column(db.VARCHAR(70))
    password = db.Column(db.VARCHAR(192))
    phone_number = db.Column(db.VARCHAR(14))
    email_or_phone = db.Column(db.Boolean, default=True)
    status = db.Column(db.Integer)  

    def __init__(self, uuid, first_name, last_name, email_address, password, phone_number, email_or_phone, status):
        self.uuid = uuid
        self.first_name = first_name
        self.last_name = last_name
        self.email_address = email_address
        self.password = password
        self.phone_number = phone_number
        self.email_or_phone = email_or_phone
        self.status = status

    def find_everyone():

        return UserModel.query.all()

    def find_by_username(username):

        return UserModel.query.filter_by(username=username).first()

    
    def find_by_email(email_address):

        return UserModel.query.filter_by(email_address=email_address).first()

    def find_by_uuid(uuid):

        return UserModel.query.filter_by(uuid=uuid).first()

    def check_uuid(uuid):
        if UserModel.query.filter_by(uuid=uuid).first:
            return False

        return True
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()

class UserSchema(ma.Schema):    
    class Meta:
        # Fields to expose.
        fields = ("uuid", "first_name", "last_name", "email_address", "status")

User_Schema = UserSchema()
Users_Schema = UserSchema(many = True)
