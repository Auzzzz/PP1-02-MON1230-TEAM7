from lib.o import db, ma

class DeviceRulesModel(db.Model):
    __tablename__ = "device_rules"
    uuid = db.Column(db.Integer, primary_key = True, autoincrement=True)
    device_uuid = db.Column(db.VARCHAR(50))
    ap_id = db.Column(db.VARCHAR(191))
    comm_type = db.Column(db.String(10))

    def __init__(self, uuid, device_uuid, ap_id, comm_type):
        self.uuid = uuid
        self.device_uuid = device_uuid
        self.ap_id = ap_id
        self.comm_type = comm_type
   

    def find_all():

        return DeviceRulesModel.query.all()

    def find_by_device_uuid(device_uuid):

        return DeviceRulesModel.query.filter_by(device_uuid=device_uuid).first()

    def find_by_comm_type(type):

        return DeviceRulesModel.query.filter_by(comm_type=type).all()

    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()