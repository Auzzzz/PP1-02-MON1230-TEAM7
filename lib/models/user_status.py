from lib.o import db, ma
from flask import jsonify

class User_StatusModel(db.Model):
    __tablename__ = "user_status"
    uuid = db.Column(db.Integer , primary_key = True)
    status_name = db.Column(db.VARCHAR(50))
    status_desc = db.Column(db.VARCHAR(400))

    def __init__(self, uuid, status_name, status_desc):
        self.uuid = uuid
        self.status_name = status_name
        self.status_desc = status_desc


    def show_all():

        return User_StatusModel.query.all()
        

    def check_uuid(uuid):
        if User_StatusModel.query.filter_by(uuid=uuid).first:
            return False

        return True
    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
    
    def deleteme(self):

        db.session.delete(self)
        db.session.commit()
