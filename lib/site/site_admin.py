from flask import Flask, Blueprint, request, jsonify, render_template, g, url_for, session, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os, requests, json, datetime
from passlib.hash import sha256_crypt
from datetime import datetime as dt
# Models
from lib.models.user_model import UserModel as M
from lib.models.user_status import User_StatusModel as US
from lib.models.user_tokens import User_TokensModel as UT
from lib.site.admin_functions import AdminFunc as AF
from lib.site.user_functions import UsersFunc as UF
from lib.models.ticket_model import TicketModel as TM
from lib.models.ticket_chat_model import TicketChatModel as TCM
from lib.models.device_rules_model import DeviceRulesModel as DRM
from lib.models.site_model import Site_Model as SM

from lib.o import scheduler
from lib.site.user_functions import UsersFunc as UF

from flask_jwt_extended import (
    create_access_token,
    get_jwt,
    get_jti
)


site_admin = Blueprint("site_admin", __name__, url_prefix='/a/')

@site_admin.route("/home", methods=['GET','POST'])
def admin_home():
    if session['status'] == 0 or 1:
        if session['status'] == 1:
            msg = ''


            # send user a txt
            if request.method == 'POST' and 'phone_number' and 'msg' in request.form :
                phone_number = request.form['phone_number']
                message = request.form['msg']

                if AF.aws_sns_send_text(phone_number, message) == True:
                    msg = "TxT message sent"
                    return render_template('admin_home.html', msg=msg)
                else:
                    msg = "An Error has occured - TxT NOT sent"
                    return render_template('admin_home.html', msg=msg)

            return render_template('admin_home.html', msg=msg)
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))


@site_admin.route("/adddevice", methods=['GET','POST'])
def add_device():
    if 'islogged' in session:
        if session['status'] == 0 or 1:

            msg =''

            all_users = M.find_everyone()    
            sites = SM.show_all()
            expires = datetime.timedelta(days=0) 

            if request.method == 'POST' and 'location' and 'time_limit' and 'uuid' and 'site' in request.form:
                location = request.form['location']
                owner_uuid = request.form['uuid']
                time_limit = request.form['time_limit']
                site = request.form['site']
                uuid = UF.create_arduino_uuid()

                new_device = UT (
                    uuid = uuid,
                    owner_uuid = owner_uuid,
                    token = create_access_token(identity=uuid, expires_delta=expires, fresh=True),
                    device_site = site,
                    location = location,
                    time_limit = time_limit,
                    status = 1
                )

                try:
                    new_device.save_to_db()
                    msg = "Device Made"

                    
                    return render_template('admin_newdevice.html', msg=msg, token=new_device.token, uuid=new_device.uuid, all_users=all_users)
                
                except Exception as e:
                    msg = "An error has occured =0"
                    print(e)
                    
                    return render_template('admin_newdevice.html', msg=msg, all_users=all_users)

            return render_template('admin_newdevice.html', msg=msg, all_users=all_users, sites = sites)

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))


@site_admin.route("/devices")
def admin_devices():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ''
            # Find all users & user devices
            all_users = M.find_everyone()
            all_devices = UT.show_all()
            all_device_rules = DRM.find_all()
            all_d_r_len = len(all_device_rules)

            all_sites = SM.show_all()


            return render_template('admin_devices.html', msg=msg, all_devices=all_devices, all_users=all_users, all_device_rules=all_device_rules, all_d_r_len = all_d_r_len, all_sites = all_sites)

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))
        

@site_admin.route("/devices/alert", methods=['GET','POST'])
def admin_devices_alert():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ''
            print(session)
            # Find all users & user devices
            if request.method == 'POST' and 'uuid' and 'choice' in request.form:
                uuid = request.form['uuid']
                choice = request.form['choice']

                device = UT.find_by_uuid(uuid)
                time_limit = device.time_limit

                sched_id = uuid + "_" + choice

                device_rule = DRM(0,
                    uuid,
                    sched_id,
                    choice)

                if choice == 'sms':
                    phone_number = session['phone']
                    device_location = device.location

                    try:
                        scheduler.add_job(UF.add_job, 'interval', seconds=time_limit, id=sched_id, args=[uuid], misfire_grace_time=1000)
                        device_rule.save_to_db()
                       
                        UF.aws_sns_send_text_test(phone_number, device_location)
                        
                    except Exception as e:
                        msg = "An error has occured =0"
                        print(e)

                else:
                    print("send email")
            
            return redirect(url_for('site_admin.admin_devices'))

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/devices/alert/remove", methods=['GET','POST'])
def admin_devices_alert_remove():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ''
            # Find all users & user devices
            if request.method == 'POST' and 'uuid' and 'choice' in request.form:
                uuid = request.form['uuid']
                choice = request.form['choice']


                if choice == 'sms':
                    sched_id = uuid + "_" + choice

                    try:
                        device = DRM.find_by_device_uuid(uuid)
                        device.deleteme()
                       
                        scheduler.remove_job(sched_id)

                    except Exception as e:
                        msg = "An error has occured =0"
                        print(e)

                else:
                    print("send email")
            
            return redirect(url_for('site_admin.admin_devices'))

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/devices/suspend", methods=['POST'])
def admin_devices_suspend():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            if request.method == 'POST' and 'token' and 'uuid' in request.form:
                token = request.form['token']
                jti = get_jti(token)
                uuid = request.form['uuid']

                if AF.blacklist_token(jti) == True:
                    AF.update_device_status(uuid, 2)
                    return redirect(url_for('site_admin.admin_devices'))
                else:
                    print(" suspend error")

                return redirect(url_for('site_admin.admin_devices'))
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/devices/reinstate", methods=['POST'])
def admin_devices_reinstate():
    if session['status'] == 0 or 1:
        if session['status'] == 1:
            if request.method == 'POST' and 'token' and 'uuid' in request.form:
                token = request.form['token']
                jti = get_jti(token)
                uuid = request.form['uuid']
                
                if AF.unblacklist_token(jti) == True:
                    AF.update_device_status(uuid, 1)
                    return redirect(url_for('site_admin.admin_devices'))
                else:
                    print("reinstate error")

                return redirect(url_for('site_admin.admin_devices'))
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))



@site_admin.route("/devices/delete", methods=['POST'])
def admin_devices_delete():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            if request.method == 'POST' and 'token' and 'uuid' in request.form:
                token = request.form['token']
                uuid = request.form['uuid']
                jti = get_jti(token)

                # Blacklist token then delete device
                if AF.blacklist_token(jti) == True:
                    if AF.delete_device(uuid) == True:
                        return redirect(url_for('site_admin.admin_devices'))
            

                return redirect(url_for('site_admin.admin_devices'))
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))


@site_admin.route("/users")
def admin_users():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ""
            users = M.find_everyone()
            user_status = US.show_all()

            for each in user_status:
                print(type(int(each.uuid)))

            for u in users:
                print(u)
                for each in user_status:
                    if u.status == int(each.uuid):
                        print(each.status_name)


            return render_template('admin_all_users.html', msg=msg, users=users, user_status = user_status)

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/users/delete", methods=['POST'])
def admin_users_delete():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            users = M.find_everyone()
            if request.method == 'POST' and 'uuid' in request.form:
                uuid = request.form['uuid']                   

                try:
                    # Get all connected devices
                    devices = UT.check_owner(uuid)

                    # Blacklist device tokens & Delete device
                    for each in devices:
                        
                        # Blacklist token then delete device
                        if AF.blacklist_token(each.token) == True:
                            if AF.delete_device(each.uuid) == True:

                                # Delete User
                                user = M.find_by_uuid(uuid)
                                user.deleteme()
                                msg = 'User Deleted'
                    return render_template('admin_all_users.html', msg=msg, users=users)
                    
                except Exception as e:
                    msg = "An error has occured"
                    emsg = e
                    print(e)             

                    return render_template('admin_all_users.html', msg=msg, users=users)
                
            else:
                return redirect(url_for('site_admin.admin_users'))
            

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/users/create", methods=['POST','GET'])
def admin_create_user():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg=''
            roles = US.show_all()

            if request.method == 'POST' and 'first_name' and 'phone' and 'last_name' and 'email' and 'password' and 'role' in request.form:
                first_name = request.form['first_name']
                last_name = request.form['last_name']
                email = request.form['email']
                password = request.form['password']
                role = request.form['role']
                phone = request.form['phone']
                
                # Check if admin is creating a super user
                if session['status'] == 1 and int(role) == 0:
                    msg = 'Admin can not create a super user'
                    return render_template('admin_register.html', msg=msg, roles = roles)
                
            
                if M.find_by_email(email) == None:
                    new_user = M(
                        uuid = UF.create_uuid(),
                        first_name = first_name,
                        last_name = last_name,
                        email_address = email,
                        password = UF.hash_pass(password),
                        email_or_phone = False,
                        phone_number = phone,
                        status = 2
                    )
                    try:
                        #new_user.save_to_db()
                        msg = "User created"
                        return render_template('admin_register.html', msg=msg, roles = roles)
                    
                    except Exception as e:
                        msg = "An error has occured"
                        emsg = e
                        return render_template('admin_register.html', msg=msg, roles = roles)
                
                else:
                    msg = "Email alreay in use"

                    return render_template('admin_register.html', msg=msg, roles = roles)

            return render_template('admin_register.html', msg=msg, roles=roles)
            
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))


@site_admin.route("/test_email", methods=['POST', 'GET'])
def admin_send_email():
    

    AF.send_email()
    return "Hi"


@site_admin.route("/tickets", methods=['POST','GET'])
def admin_ticket():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ""
            users = M.find_everyone()
            tickets = TM.find_all()
            


            return render_template('admin_tickets.html', msg=msg, tickets=tickets, users = users)

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/ticket/read", methods=['POST','GET'])
def admin_ticket_read():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ""
            if request.method == 'POST' and 'uuid' in request.form:
                uuid = request.form['uuid']

                tickets = TM.find_by_uuid(uuid)
                ticket_info = TCM.find_by_ticket_uuid(uuid)
                users = M.find_everyone()   
                t_i = len(ticket_info)



                return render_template('admin_ticket_reply.html', msg = msg, session=session, tickets = tickets, ticket_info = ticket_info, users = users, t_i=t_i)
        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site_admin.route("/ticket/reply", methods=['POST','GET'])
def admin_ticket_reply():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = ""
            tickets = TM.find_by_user_uuid(session['uuid'])
            if request.method == 'POST' and 'ticket_uuid' and 'ticket_body' and 'user_uuid' in request.form:
                ticket_uuid = request.form['ticket_uuid']   
                ticket_body = request.form['ticket_body']
                user_uuid = request.form['user_uuid']
                time = dt.now()

                ticket_reply = TCM(uuid=0,
                ticket_uuid= ticket_uuid,
                user_uuid=user_uuid,
                date_time=time,
                ticket_reply=ticket_body)

                try:
                    ticket_reply.save_to_db()
                    msg='Ticket Reply saved'

                    return redirect(url_for('site_admin.admin_ticket'))    

                except Exception as e:
                        msg = "An error has occured =0"
                        print(e)
                        
                        return render_template('admin_ticket.html', msg = msg, session=session, tickets = tickets)
            
            return render_template('admin_ticket.html', msg = msg, session=session, tickets = tickets)

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))



@site_admin.route("/ticket/reopen", methods=['POST','GET'])
def admin_ticket_reopen():
    if 'islogged' in session:
        if session['status'] == 0 or 1:
            msg = 'Ticket Closed'
            tickets = TM.find_by_user_uuid(session['uuid'])
            if request.method == 'POST' and 'uuid' in request.form:
                uuid = request.form['uuid']

                ticket = TM.find_by_uuid(uuid)
                ticket.ticket_status = 2
                ticket.save_to_db()
            

            return redirect(url_for('site_admin.admin_ticket'))  

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))  

@site_admin.route("/alljobs", methods=['POST','GET'])
def alljobs():
    if 'islogged' in session:
        
        for job in scheduler.get_jobs():
            
            print("name: %s, trigger: %s, next run: %s, handler: %s" % (job.name, job.trigger, job.next_run_time, job.func))

            return ""

        else:
            return redirect(url_for('site.home'))

    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))