from lib.models.user_model import UserModel as M
from lib.models.data_model import DataModel as DM
from lib.models.user_tokens import User_TokensModel as UTM
from lib.models.user_tokens import User_TokensModel as UT
from lib.o import jwt
from flask import jsonify, redirect, url_for, request
from datetime import time, datetime
from pychartjs import BaseChart, ChartType, Color  
import uuid, re, hashlib, binascii, os, boto3, requests, csv, random


class UsersFunc():

    def create_uuid():
    # Create and check the uuid is not in use,
    # contiune to loop through until find_by_uuid returns True
        while True:
            new_uuid = str(uuid.uuid4())
            if M.check_uuid(uuid):
                continue
            break

        return new_uuid

    def create_arduino_uuid():
    # Create and check the uuid is not in use,
    # contiune to loop through until find_by_uuid returns True
        while True:
            new_uuid = str(uuid.uuid1())
            if M.check_uuid(uuid):
                continue
            break

        return new_uuid

    def hash_pass(password):

        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')

        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)

        return (salt + pwdhash) # return bytes

    def verify_pass(provided_password, stored_password):
        # Get the stored salt and password from store
        salt = stored_password[:64]
        stored_password = stored_password[64:]

        pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')

        if pwdhash == stored_password:
            return True
        else:
            return False
    
    def aws_sns_send_text(phone_number, device_location):
        try:
            sns = boto3.client("sns", 
                        region_name="ap-southeast-1", 
                        aws_access_key_id="AKIAYIF6IJHLFRMJYOEM", 
                        aws_secret_access_key="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")

            # Send a single SMS (no topic, no subscription needed)
            msg = " Device " + device_location + "has not uploaded data at the specified time"
            phone_number = "+" + phone_number

            sns.publish(PhoneNumber=phone_number, Message=msg)

            return True

        except Exception as e:
                msg = "An error has occured =0"
                print(e)
                
                return e
    
    def aws_sns_send_text_test(phone_number, device_location):
        try:
            sns = boto3.client("sns", 
                        region_name="ap-southeast-1", 
                        aws_access_key_id="AKIAYIF6IJHLFRMJYOEM", 
                        aws_secret_access_key="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")

            # Send a single SMS (no topic, no subscription needed)
            msg = " This is a test message for Device "
            phone_number = "+" + phone_number

            sns.publish(PhoneNumber=phone_number, Message=msg)

            return True

        except Exception as e:
                msg = "An error has occured =0"
                print(e)
                
                return e
    
    ## Add Job ##
    def add_job(uuid):

        payload = {"uuid":uuid}
        r = requests.post('http://127.0.0.1:4000/knjbhvghvn243bv8657muy346521trfe4gvd2snb42vf3rg2ef', json=payload)
        
        return ""
    
    ## Email ##

    def data_upload_check(device_uuid):

        try:
            # Get the device details to get user details
            device = UTM.find_by_uuid(device_uuid)
            owner_uuid = device.owner_uuid
            owner = M.find_by_uuid(owner_uuid)

            # Get the last uploaded value time for the device
            last_upload = DM.last_upload(device_uuid).date_time
            print(last_upload)

            # Check the time diffrence in minitues to compare to user settings
            if device.time_limit >= UsersFunc.day_time_diff(last_upload):
                return False
            else:   
                # Email = False
                if owner.email_or_phone == True:
                    # Send Email
                    print("EMAIL")
                else:

                    UsersFunc.aws_sns_send_text(owner.phone_number, device.location)
    
            return True
        
        except Exception as e:
                msg = "An error has occured =0"
                print(e)
                
                return e

    def day_time_diff(last_upload):
        # Get the current time and compare dates, then look at how many
        # minitues has passed for comparsion
        now = datetime.now()
        diffrence = now - last_upload
        mins = diffrence.total_seconds()/60

        return mins

    def chart(uuid):
        # Predefined choice
        colours = ["Aqua","Tomato","Orange","DodgerBlue","MediumSeaGreen","Gray","SlateBlue","Violet","LightGray"]

        # Get all the sensor data
        graph_data = DM.graph_data(uuid)
    
        if graph_data == None:
            return "error"
        # Get the sensor names
        sensor_list = UT.find_by_uuid(uuid)
        sensor_list = sensor_list.sensor_names
        sensor_list = [x.strip() for x in str(sensor_list).split(',')]
        sensor_list_len = len(sensor_list)
        

        top = "new Chart('myChart', { type: 'line', data: { labels: xValutopes, data: { datasets: ["
        bottom = "]},options: {'legend: {'display: false};}'});"

        data = {}
        list = []
        colour = {}
        datasets = []
        count = 0

        
        while count < sensor_list_len:
            for each_list in sensor_list:
                # assign key to the name of the sensor
                key = each_list

                # for each key get all the graph data placing it in a list indise
                for each in graph_data:
                    if each.sensor_name == key:
                        list.append(int(each.value))
                
                data[key] = list
                #empty the list
                list = []
                count += 1

                # Get the list from the data array
                y = data[key]
                # take the data turn it into a string with , seperating
                x = ','.join([str(elem) for elem in y])
                # add [] to the ends
                #end = "{data: [" + x + "], borderColor:'" + random.choice(colours) + "', fill: false},"
                end =  "[" + x + "]"
                # add into datasets
                datasets.append(end)
        #print(datasets)
        
        data = ''
        i = len(datasets)
        x = 0
        while x < i:
            data += datasets[x]
            x += 1
        


        return datasets