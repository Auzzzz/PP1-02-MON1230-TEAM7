from lib.models.user_model import UserModel as M
from lib.o import jwt
from datetime import date
import boto3, requests
# Models
from lib.models.user_model import UserModel as M
from lib.models.user_status import User_StatusModel as US
from lib.models.user_tokens import User_TokensModel as UT
from lib.models.user_tokens_revoked import UserRevokedTokensModel as UR

class AdminFunc():
    def delete_device(device_uuid):
        try:
           device = UT.find_by_uuid(device_uuid)
           device.deleteme()

           return True

        except Exception as e:
            msg = "An error has occured =0"
            print(e)
            
            return e

    def blacklist_token(jti):
            try:
                
                token = UR(jti = jti, date = date.today())
                token.save_to_db()

                return True
            except Exception as e:
                print(e)

                return e

    def unblacklist_token(jti):
            try:
                
                token = UR.check_token(jti)
                token.deleteme()

                return True

            except Exception as e:
                print(e)

                return e

    def check_token(jti):

        return UR.check_token(jti)

    def update_device_status(uuid, value):
        
        return UT.update(uuid, value)

    
    def send_email():
        try:
            api_key = "102dfab4298c19c180972780b9e97cfd-4b1aa784-e2a31283"

            return requests.post(
                "https://api.mailgun.net/v3/sandboxe69f2ff3a17842f592fba41b82f07f11.mailgun.org/messages",
                auth=("api", api_key),
                data={"from": "Excited User <mailgun@sandboxe69f2ff3a17842f592fba41b82f07f11.mailgun.org>",
                    "to": ["http://bin.mailgun.net/d70736e1"],
                    "subject": "Hello",
                    "text": "Testing some Mailgun awesomness!"})

        except Exception as e:
                msg = "An error has occured =0"
                print(e)
                
                return e