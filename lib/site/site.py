from flask import Flask, Blueprint, request, redirect, jsonify, render_template, g, url_for, session
from flask.helpers import send_file, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os, requests, json, time, csv, boto3, random
from datetime import datetime
from passlib.hash import sha256_crypt
from collections.abc import Iterable

# Models
from lib.models.user_model import UserModel as M
from lib.models.user_status import User_StatusModel as US
from lib.models.user_tokens import User_TokensModel as UT
from lib.models.ticket_model import TicketModel as TM
from lib.models.ticket_chat_model import TicketChatModel as TCM
from lib.models.data_model import DataModel as DM
from lib.models.site_model import Site_Model as SM
from lib.site.user_functions import UsersFunc as UF
from lib.o import db, ma, scheduler

site = Blueprint("site", __name__)

# Temp Endpoint
@site.route("/create")
def dbcreation():

    try:
        print(M.find_everyone())

        return redirect(url_for('site.login'))
    except Exception as e:
        db.create_all() 

        new_user = M(
                    uuid = UF.create_uuid(),
                    first_name = "Admin",
                    last_name = "Admin",
                    email_address = "Admin@test.com",
                    password = UF.hash_pass("ChangeMe"),
                    phone_number = "",
                    email_or_phone = False,
                    status = 1
                )

        try:
            new_user.save_to_db()
            msg = "User created"
            return jsonify(msg = "Admin Account created, ensure you create your own then delete this account later", email = new_user.email_address, password = "ChangeMe" )
                
        except Exception as e:
            msg = "An error has occured"
            emsg = e
            return render_template('register.html', msg=msg, emsg=msg)



# Check DB for schduler
@site.route("/knjbhvghvn243bv8657muy346521trfe4gvd2snb42vf3rg2ef", methods=['POST'])
def check():
    # Read JSON data from post
    data = request.get_json()

    # Break down JSON data
    uuid = data['uuid']

    UF.data_upload_check(uuid)

    return "hi"
    

## Home ##
@site.route("/home", methods=['GET','POST'])
def home():
    #uuid = "a8d8c4ea-b14e-11eb-8bef-dddd788eeccb"
    #scheduler_id = 
    #scheduler.add_job(printing_something, 'interval', seconds=5, id='', args=[uuid], misfire_grace_time=1000)

    user_devices = UT.check_owner(session['uuid'])
    all_sites = SM.show_all()

    if session.get('islogged'):
        return render_template('index.html', session = session, user_devices = user_devices, all_sites = all_sites)
    else:
        return redirect(url_for('site.login'))


## Register ##
@site.route("/register", methods=['GET','POST'])
def register():
    msg = ''
    
    # get the user status from db
    status = US.show_all()
    
    if request.method == 'POST' and 'first_name' and 'last_name' and 'email' and 'password' and 'phone' in request.form:
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        phone = request.form['phone']
        password = request.form['password']
    
        if M.find_by_email(email) == None:
            new_user = M(
                uuid = UF.create_uuid(),
                first_name = first_name,
                last_name = last_name,
                email_address = email,
                password = UF.hash_pass(password),
                phone_number = phone,
                email_or_phone = False,
                status = 2
            )

            try:
                new_user.save_to_db()
                msg = "User created"
                return render_template('register.html', msg=msg, status = status)
            
            except Exception as e:
                msg = "An error has occured"
                emsg = e
                return render_template('register.html', msg=msg, emsg=msg)

        else:
            msg = "Email alreay in use"

            return render_template('register.html', msg=msg, status=status)



    else:
        return render_template('register.html', msg=msg, status=status)

    return render_template('register.html', msg=msg, status=status)


## Login ##
@site.route("/login", methods=['GET','POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'email' in request.form and 'password' in request.form:
        #Capture the form data
        email = request.form['email']
        password = request.form['password']
    
        # Check if username is stored, return error msg
        current_user = M.find_by_email(email)
        if current_user == None:
            msg = "Email or Password are incorrect"

            return render_template('login.html', msg = msg)


        else:
            if UF.verify_pass(password, current_user.password):
                    msg = "logged in"

                    session['islogged'] = True
                    session['uuid'] = current_user.uuid
                    session['first_name'] = current_user.first_name
                    session['status'] = current_user.status
                    session['phone'] = current_user.phone_number

                    return redirect(url_for('site.home'))
    
            else:
                msg = "username or password are incorrect"
                return render_template('login.html', msg = msg)


    return render_template('login.html', msg = msg)

    
## Logout ##
@site.route('/logout')
def logout():
    
    if 'islogged' in session:
        session.clear()
    else:
        msg = "Not signed in"
        return render_template('login.html', msg = msg)

    msg = "Logged off"
    return redirect(url_for('site.login'))


## Submit Ticket ##
@site.route('/ticket', methods=['GET','POST'])
def ticket():
    if 'islogged' in session:
        msg = ''
        tickets = TM.find_by_user_uuid(session['uuid'])
        if request.method == 'POST' and 'ticket_header' and 'ticket_body' and 'user_uuid' in request.form:
            ticket_header = request.form['ticket_header']   
            ticket_body = request.form['ticket_body']
            user_uuid = request.form['user_uuid']
            time = datetime.now()

            ticket = TM(uuid=0,
            user_uuid=user_uuid,
            date_time=time,
            ticket_header=ticket_header,
            ticket_body=ticket_body,
            ticket_status = 2)

            try:
                ticket.save_to_db()
                msg='Ticket Created' + ticket.uuid

                return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

            except Exception as e:
                    msg = "An error has occured =0"
                    print(e)
                    
                    return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

    else:
        msg = "Not signed in"
        redirect(url_for('site.login'))

    return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

## ticket read ##
@site.route("/ticket/read", methods=['GET','POST'])
def ticket_read():

    if session.get('islogged'):
        msg = ''
        if request.method == 'POST' and 'uuid' in request.form:
            uuid = request.form['uuid']

            tickets = TM.find_by_uuid(uuid)
            ticket_info = TCM.find_by_ticket_uuid(uuid)
            users = M.find_everyone()   
            t_i = len(ticket_info)



            return render_template('user_ticket_reply.html', msg = msg, session=session, tickets = tickets, ticket_info = ticket_info, users = users, t_i=t_i)
                
    else:
        return redirect(url_for('site.login'))

@site.route('/ticket/reply', methods=['GET','POST'])
def ticket_reply():
    if 'islogged' in session:
        msg = ''
        tickets = TM.find_by_user_uuid(session['uuid'])
        if request.method == 'POST' and 'ticket_uuid' and 'ticket_body' and 'user_uuid' in request.form:
            ticket_uuid = request.form['ticket_uuid']   
            ticket_body = request.form['ticket_body']
            user_uuid = request.form['user_uuid']
            time = datetime.now()

            ticket_reply = TCM(uuid=0,
            ticket_uuid= ticket_uuid,
            user_uuid=user_uuid,
            date_time=time,
            ticket_reply=ticket_body)

            try:
                ticket_reply.save_to_db()
                msg='Ticket Reply saved'

                return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

            except Exception as e:
                    msg = "An error has occured =0"
                    print(e)
                    
                    return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

    else:
        msg = "Not signed in"
        redirect(url_for('site.login'))

    return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)

## ticket close ##
@site.route("/ticket/close", methods=['GET','POST'])
def ticket_close():

    if session.get('islogged'):
        msg = 'Ticket Closed'
        tickets = TM.find_by_user_uuid(session['uuid'])
        if request.method == 'POST' and 'uuid' in request.form:
            uuid = request.form['uuid']

            ticket = TM.find_by_uuid(uuid)
            ticket.ticket_status = 1
            ticket.save_to_db()
        

        return render_template('user_submit_ticket.html', msg = msg, session=session, tickets = tickets)
    else:
        return redirect(url_for('site.login'))



## New Rule on device ##

@site.route('/device/rule', methods=['GET','POST'])
def device_rule():
    if 'islogged' in session:
        msg = ''
        if request.method == 'POST' and '' and '' and '' in request.form:
            ticket_header = request.form['ticket_header']
            ticket_body = request.form['ticket_body']
            user_uuid = request.form['user_uuid']
            time = datetime.now()
        
    else:
        msg = "Not signed in"
        redirect(url_for('site.login'))

    return render_template('user_submit_ticket.html', msg = msg, session=session)

## Device Data ##

@site.route('/data', methods=['GET','POST'])
def data():
    if 'islogged' in session:
        msg = ''
        if request.method == 'POST' and 'uuid' in request.form:
            uuid = request.form['uuid']
            chart = UF.chart(uuid)
            chart = chart[:5]        

            sensor_list = UT.find_by_uuid(uuid)
            #print(type(sensor_list.sensor_names))
            sensor_list = UT.find_by_uuid(uuid)
            sensor_list = sensor_list.sensor_names
            #sensor_list = [x.strip() for x in str(sensor_list).split(',')]
            sensor_list_len = len(sensor_list)
            
            print(sensor_list[0])

            return render_template('user_chart.html', chart = chart, labels = sensor_list)
            
        
    else:
        msg = "Not signed in"
        redirect(url_for('site.login'))
    return render_template('user_submit_ticket.html', msg = msg, session=session)

@site.route('/download_csv', methods=['GET','POST'])
def download_csv():
    if 'islogged' in session:
        msg = ''
        if request.method == 'POST' and 'uuid' in request.form:    
            uuid = request.form['uuid']

            data = DM.graph_data(uuid)

            if data is None:
                print("error")
            else:
                with open("./lib/"+uuid + ".csv", mode='w') as output_file:
                    writer = csv.writer(output_file, delimiter = ",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

                    for each in data:
                        writer.writerow([each.uuid, each.date_time, each.sensor_name, each.value])
                r = { }
                return send_file(uuid + ".csv", as_attachment=True), os.remove('./lib/' + uuid + ".csv")

              
    else:
        msg = "Not signed in"
        return redirect(url_for('site.login'))

@site.route('/test', methods=['GET','POST'])
def test():
    scheduler.add_job(temp, 'interval', seconds=10, id='', misfire_grace_time=1000)
    
    return ""

def temp():
    payload = {
        "light_sensor":random.randint(50,700),
        "temp_sensor":random.randint(15,20),
        "humiid_sensor":random.randint(0,24),
        "plant001_sensor":random.randint(30,50)
        }
    r = requests.post('https://plantapp-deploy.herokuapp.com/api/v1/input', headers={'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjI0NzE2OTQxLCJqdGkiOiIxZTY1NDU3Yy1hNzIzLTQ2MTYtOTlhMS01NGY1ZTMwZTI4OTQiLCJuYmYiOjE2MjQ3MTY5NDEsInR5cGUiOiJhY2Nlc3MiLCJzdWIiOiJmZDJlNWI0YS1kNjg4LTExZWItODdjMi1jM2Y5NTFhMjU4YzcifQ.QDWI8rjaieJJF1jnlm90WeumB9YC-8R1--TRGhKh13Q'}, json=payload)
    print("yes")
    r
