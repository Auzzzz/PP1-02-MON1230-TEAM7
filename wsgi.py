from flask import Flask, render_template
from lib.o import jwt, app
from lib.api.api_users import api_users
from lib.site.site import site
from lib.site.site_admin import site_admin
from lib.site.admin_functions import AdminFunc as AF

# Create Flask App
app.register_blueprint(api_users)
app.register_blueprint(site)
app.register_blueprint(site_admin)

# Check for blacklited tokens

@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload):
    jti = jwt_payload["jti"]
    print(jti)
    return AF.check_token(jti)


if __name__ == "__main__":
    app.run(host = "127.0.0.1", port = 4000, debug = True)
    